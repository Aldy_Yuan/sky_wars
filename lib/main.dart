import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:sky_wars/app/bindings/db_binding.dart';
import 'package:sky_wars/app/bindings/user_binding.dart';
import 'package:sky_wars/app/data/services/firebase_service.dart';

import 'app/controllers/user_controller.dart';
import 'app/pages/login_page.dart';
import 'app/pages/main_page.dart';
import 'app/routes/pages.dart';

Future<void> initServices() async {
  await Get.putAsync(() => FirebaseService().init());
  await DBControllerBinding().dependencies();
  UserBinding().dependencies();
}

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await initServices();

  runApp(
    GetMaterialApp(
      debugShowCheckedModeBanner: false,
      themeMode: ThemeMode.light,
      title: "Sky Wars App",
      initialRoute: '/',
      getPages: AppPages.pages,
      theme: ThemeData(
        brightness: Brightness.light,
        primaryColor: Colors.blue,
        scaffoldBackgroundColor: Colors.white,
        cardTheme: CardTheme(
          elevation: 4,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(12.0),
          ),
        ),
        inputDecorationTheme: const InputDecorationTheme(
          filled: true,
        ),
        colorScheme: ColorScheme.fromSwatch().copyWith(secondary: Colors.blue),
        elevatedButtonTheme: ElevatedButtonThemeData(
          style: ButtonStyle(
            shape: MaterialStateProperty.all<OutlinedBorder>(
              RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(32.0),
              ),
            ),
            padding: MaterialStateProperty.all<EdgeInsetsGeometry>(
              const EdgeInsets.all(14.0),
            ),
            backgroundColor: MaterialStateProperty.all<Color>(Colors.blue),
            textStyle: MaterialStateProperty.all<TextStyle>(
              const TextStyle(fontWeight: FontWeight.bold),
            ),
          ),
        ),
      ),
      locale: Get.deviceLocale,
      builder: (context, child) => child!,
    ),
  );
}

class MyApp extends GetView<UserController> {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return controller.obx(
      (state) {
        return const MainPage();
      },
      onLoading: const Scaffold(
        body: Center(
          child: CircularProgressIndicator(),
        ),
      ),
      onEmpty: const LoginPage(),
      onError: (error) => Scaffold(
        body: Center(
          child: Text("$error"),
        ),
      ),
    );
  }
}
