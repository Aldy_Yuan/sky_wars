import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:sky_wars/app/bindings/detail_binding.dart';
import 'package:sky_wars/app/controllers/people_controller.dart';
import 'package:sky_wars/app/data/models/people/people.dart';
import 'package:sky_wars/app/pages/detail_people_page.dart';

class PeopleGrid extends StatelessWidget {
  final People people;
  const PeopleGrid({
    Key? key,
    required this.people,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final PeopleController _peopleController = Get.find<PeopleController>();

    return InkWell(
      onTap: () => Get.to(
        () => DetailPeoplePage(people: people),
        binding: DetailBinding(),
      ),
      child: Card(
        child: GridTile(
          child: Padding(
            padding: const EdgeInsets.all(4.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                const SizedBox(
                  height: 4,
                ),
                Text(
                  people.name!,
                  style: const TextStyle(fontWeight: FontWeight.bold),
                  textAlign: TextAlign.center,
                ),
                Text(
                  people.gender ?? '',
                  style: const TextStyle(fontWeight: FontWeight.w300),
                  textAlign: TextAlign.center,
                ),
                IconButton(
                  onPressed: () async {
                    if (people.favorite == 0) {
                      _peopleController.addFavorite(people);
                    } else {
                      _peopleController.deleteFavorite(people);
                    }
                  },
                  icon: people.favorite == 0
                      ? const Icon(Icons.favorite_border_outlined)
                      : const Icon(
                          Icons.favorite,
                          color: Colors.red,
                        ),
                  alignment: Alignment.centerRight,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
