import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:sky_wars/app/bindings/detail_binding.dart';
import 'package:sky_wars/app/controllers/people_controller.dart';
import 'package:sky_wars/app/data/models/people/people.dart';
import 'package:sky_wars/app/pages/detail_people_page.dart';

class PeopleCard extends StatelessWidget {
  final People people;
  const PeopleCard({
    Key? key,
    required this.people,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final PeopleController _peopleController = Get.find<PeopleController>();

    return Card(
      child: ListTile(
        title: Text(people.name!),
        subtitle: Text(people.gender ?? ''),
        leading: IconButton(
          onPressed: () async {
            if (people.favorite == 0) {
              _peopleController.addFavorite(people);
            } else {
              _peopleController.deleteFavorite(people);
            }
          },
          icon: people.favorite == 0
              ? const Icon(Icons.favorite_border_outlined)
              : const Icon(
                  Icons.favorite,
                  color: Colors.red,
                ),
        ),
        trailing: IconButton(
          onPressed: () {
            Get.to(
              () => DetailPeoplePage(people: people),
              binding: DetailBinding(),
            );
            _peopleController.search("");
            _peopleController.text.clear();
            FocusScope.of(context).unfocus();
          },
          icon: const Icon(Icons.navigate_next_rounded),
        ),
      ),
    );
  }
}
