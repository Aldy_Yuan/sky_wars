import 'package:json_annotation/json_annotation.dart';

part 'planet.g.dart';

@JsonSerializable()
class Planet {
  @JsonKey(name: "name")
  final String? name;

  @JsonKey(name: "rotation_period")
  final String? rotationPeriod;

  @JsonKey(name: "orbital_period")
  final String? orbitalPeriod;

  @JsonKey(name: "diameter")
  final String? diameter;

  @JsonKey(name: "climate")
  final String? climate;

  @JsonKey(name: "gravity")
  final String? gravity;

  @JsonKey(name: "terrain")
  final String? terrain;

  @JsonKey(name: "surface_water")
  final String? surfaceWater;

  @JsonKey(name: "population")
  final String? population;

  @JsonKey(name: "residents")
  final List<String>? residents;

  @JsonKey(name: "films")
  final List<String>? films;

  @JsonKey(name: "url")
  final String? url;

  const Planet(
    this.name,
    this.rotationPeriod,
    this.orbitalPeriod,
    this.diameter,
    this.climate,
    this.gravity,
    this.terrain,
    this.surfaceWater,
    this.population,
    this.residents,
    this.films,
    this.url,
  );

  factory Planet.fromJson(Map<String, dynamic> json) => _$PlanetFromJson(json);

  Map<String, dynamic> toJson() => _$PlanetToJson(this);
}
