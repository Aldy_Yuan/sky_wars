import 'package:json_annotation/json_annotation.dart';

part 'species.g.dart';

@JsonSerializable()
class Species {
  @JsonKey(name: "name")
  final String? name;

  @JsonKey(name: "classification")
  final String? classification;

  @JsonKey(name: "designation")
  final String? designation;

  @JsonKey(name: "average_height")
  final String? averageHeight;

  @JsonKey(name: "skin_colors")
  final String? skinColors;

  @JsonKey(name: "hair_colors")
  final String? hairColors;

  @JsonKey(name: "eye_colors")
  final String? eyeColors;

  @JsonKey(name: "average")
  final String? averageLifespan;

  @JsonKey(name: "homeworld")
  final String? homeworld;

  @JsonKey(name: "language")
  final String? language;

  @JsonKey(name: "people")
  final List<String>? people;

  @JsonKey(name: "films")
  final List<String>? films;

  @JsonKey(name: "url")
  final String? url;

  const Species(
    this.name,
    this.classification,
    this.designation,
    this.averageHeight,
    this.skinColors,
    this.hairColors,
    this.eyeColors,
    this.averageLifespan,
    this.homeworld,
    this.language,
    this.people,
    this.films,
    this.url,
  );

  factory Species.fromJson(Map<String, dynamic> json) =>
      _$SpeciesFromJson(json);

  Map<String, dynamic> toJson() => _$SpeciesToJson(this);
}
