// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'people.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

People _$PeopleFromJson(Map<String, dynamic> json) => People(
      json['rowid'] as int?,
      json['name'] as String?,
      json['height'] as String?,
      json['mass'] as String?,
      json['skin_color'] as String?,
      json['eye_color'] as String?,
      json['birth_year'] as String?,
      json['gender'] as String?,
      json['url'] as String?,
      favorite: json['favorite'] as int? ?? 0,
    );

Map<String, dynamic> _$PeopleToJson(People instance) => <String, dynamic>{
      'rowid': instance.id,
      'name': instance.name,
      'height': instance.height,
      'mass': instance.mass,
      'skin_color': instance.skinColor,
      'eye_color': instance.eyeColor,
      'birth_year': instance.birthYear,
      'gender': instance.gender,
      'url': instance.url,
      'favorite': instance.favorite,
    };
