import 'package:json_annotation/json_annotation.dart';

part 'people.g.dart';

@JsonSerializable()
class People {
  @JsonKey(name: "rowid")
  final int? id;

  @JsonKey(name: "name")
  final String? name;

  @JsonKey(name: "height")
  final String? height;

  @JsonKey(name: "mass")
  final String? mass;

  @JsonKey(name: "skin_color")
  final String? skinColor;

  @JsonKey(name: "eye_color")
  final String? eyeColor;

  @JsonKey(name: "birth_year")
  final String? birthYear;

  @JsonKey(name: "gender")
  final String? gender;

  @JsonKey(name: "url")
  final String? url;

  @JsonKey(name: 'favorite')
  int favorite;

  People(
    this.id,
    this.name,
    this.height,
    this.mass,
    this.skinColor,
    this.eyeColor,
    this.birthYear,
    this.gender,
    this.url, {
    this.favorite = 0,
  });

  factory People.fromJson(Map<String, dynamic> json) => _$PeopleFromJson(json);

  Map<String, dynamic> toJson() => _$PeopleToJson(this);
}
