import 'package:json_annotation/json_annotation.dart';

part 'film.g.dart';

@JsonSerializable()
class Film {
  @JsonKey(name: "title")
  final String? title;

  @JsonKey(name: "episode_id")
  final int? episodeID;

  @JsonKey(name: "opening_crawl")
  final String? openingCrawl;

  @JsonKey(name: "director")
  final String? director;

  @JsonKey(name: "producer")
  final String? producer;

  @JsonKey(name: "release_date")
  final String? releaseDate;

  @JsonKey(name: "characters")
  final List<String>? characters;

  @JsonKey(name: "planets")
  final List<String>? planets;

  @JsonKey(name: "starships")
  final List<String>? starships;

  @JsonKey(name: "vehicles")
  final List<String>? vehicles;

  @JsonKey(name: "species")
  final List<String>? species;

  @JsonKey(name: "url")
  final String? url;

  const Film(
    this.title,
    this.episodeID,
    this.openingCrawl,
    this.director,
    this.producer,
    this.releaseDate,
    this.characters,
    this.planets,
    this.starships,
    this.vehicles,
    this.species,
    this.url,
  );

  factory Film.fromJson(Map<String, dynamic> json) => _$FilmFromJson(json);

  Map<String, dynamic> toJson() => _$FilmToJson(this);
}
