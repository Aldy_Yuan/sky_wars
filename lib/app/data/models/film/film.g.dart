// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'film.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Film _$FilmFromJson(Map<String, dynamic> json) => Film(
      json['title'] as String?,
      json['episode_id'] as int?,
      json['opening_crawl'] as String?,
      json['director'] as String?,
      json['producer'] as String?,
      json['release_date'] as String?,
      (json['characters'] as List<dynamic>?)?.map((e) => e as String).toList(),
      (json['planets'] as List<dynamic>?)?.map((e) => e as String).toList(),
      (json['starships'] as List<dynamic>?)?.map((e) => e as String).toList(),
      (json['vehicles'] as List<dynamic>?)?.map((e) => e as String).toList(),
      (json['species'] as List<dynamic>?)?.map((e) => e as String).toList(),
      json['url'] as String?,
    );

Map<String, dynamic> _$FilmToJson(Film instance) => <String, dynamic>{
      'title': instance.title,
      'episode_id': instance.episodeID,
      'opening_crawl': instance.openingCrawl,
      'director': instance.director,
      'producer': instance.producer,
      'release_date': instance.releaseDate,
      'characters': instance.characters,
      'planets': instance.planets,
      'starships': instance.starships,
      'vehicles': instance.vehicles,
      'species': instance.species,
      'url': instance.url,
    };
