import 'package:json_annotation/json_annotation.dart';

part 'starship.g.dart';

@JsonSerializable()
class Starship {
  @JsonKey(name: "name")
  final String? name;

  @JsonKey(name: "model")
  final String? model;

  @JsonKey(name: "manufacturer")
  final String? manufacturer;

  @JsonKey(name: "cost_in_credits")
  final String? costInCredits;

  @JsonKey(name: "length")
  final String? length;

  @JsonKey(name: "max_atmosphering_speed")
  final String? maxAtmospheringSpeed;

  @JsonKey(name: "crew")
  final String? crew;

  @JsonKey(name: "passengers")
  final String? passengers;

  @JsonKey(name: "cargo_capacity")
  final String? cargoCapacity;

  @JsonKey(name: "consumables")
  final String? consumables;

  @JsonKey(name: "hyperdrive_rating")
  final String? hyperdriveRating;

  @JsonKey(name: "MGLT")
  final String? mglt;

  @JsonKey(name: "starship_class")
  final String? starshipClass;

  @JsonKey(name: "pilots")
  final List<String>? pilots;

  @JsonKey(name: "films")
  final List<String>? films;

  @JsonKey(name: "url")
  final String? url;

  const Starship(
    this.name,
    this.model,
    this.manufacturer,
    this.costInCredits,
    this.length,
    this.maxAtmospheringSpeed,
    this.crew,
    this.passengers,
    this.cargoCapacity,
    this.consumables,
    this.hyperdriveRating,
    this.mglt,
    this.starshipClass,
    this.pilots,
    this.films,
    this.url,
  );

  factory Starship.fromJson(Map<String, dynamic> json) =>
      _$StarshipFromJson(json);

  Map<String, dynamic> toJson() => _$StarshipToJson(this);
}
