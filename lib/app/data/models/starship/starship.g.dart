// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'starship.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Starship _$StarshipFromJson(Map<String, dynamic> json) => Starship(
      json['name'] as String?,
      json['model'] as String?,
      json['manufacturer'] as String?,
      json['cost_in_credits'] as String?,
      json['length'] as String?,
      json['max_atmosphering_speed'] as String?,
      json['crew'] as String?,
      json['passengers'] as String?,
      json['cargo_capacity'] as String?,
      json['consumables'] as String?,
      json['hyperdrive_rating'] as String?,
      json['MGLT'] as String?,
      json['starship_class'] as String?,
      (json['pilots'] as List<dynamic>?)?.map((e) => e as String).toList(),
      (json['films'] as List<dynamic>?)?.map((e) => e as String).toList(),
      json['url'] as String?,
    );

Map<String, dynamic> _$StarshipToJson(Starship instance) => <String, dynamic>{
      'name': instance.name,
      'model': instance.model,
      'manufacturer': instance.manufacturer,
      'cost_in_credits': instance.costInCredits,
      'length': instance.length,
      'max_atmosphering_speed': instance.maxAtmospheringSpeed,
      'crew': instance.crew,
      'passengers': instance.passengers,
      'cargo_capacity': instance.cargoCapacity,
      'consumables': instance.consumables,
      'hyperdrive_rating': instance.hyperdriveRating,
      'MGLT': instance.mglt,
      'starship_class': instance.starshipClass,
      'pilots': instance.pilots,
      'films': instance.films,
      'url': instance.url,
    };
