import 'package:sky_wars/app/data/models/people/people.dart';
import 'package:sky_wars/app/data/providers/user_provider.dart';

class UserRepository {
  final UserProvider _provider;

  UserRepository(this._provider);

  Future<List<People>> getFavorites() async => _provider.getFavorites();

  Future<void> saveUser({required Map<String, dynamic> user}) async =>
      _provider.saveUsers(user: user);

  Future<void> clearUser() async => await _provider.clearUsers();

  Future<void> addFavorite({
    required List<People> list,
    required People value,
  }) async =>
      _provider.addFavorite(list: list, value: value);

  Future<void> deleteFavorite({
    required List<People> list,
    required int index,
  }) async =>
      _provider.deleteFavorite(list: list, index: index);
}
