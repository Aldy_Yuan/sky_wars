import 'package:sky_wars/app/data/models/film/film.dart';
import 'package:sky_wars/app/data/providers/film_provider.dart';

class FilmRepository {
  final FilmProvider _provider;

  FilmRepository(this._provider);

  Future<List<Film>> loadData({required int id}) async =>
      await _provider.fetchData(id: id);
}
