import 'package:sky_wars/app/data/models/people/people.dart';
import 'package:sky_wars/app/data/providers/people_provider.dart';

class PeopleRepository {
  final PeopleProvider _provider;

  PeopleRepository(this._provider);

  Future<List<People>> loadData() async => await _provider.fetchData();

  Future<void> insertData(People data) async =>
      await _provider.addData(value: data);

  Future<void> updateData(People data) async =>
      await _provider.updateData(value: data);

  Future<void> deleteData(int id) async => await _provider.deleteData(id: id);
}
