import 'package:sky_wars/app/data/models/species/species.dart';
import 'package:sky_wars/app/data/providers/species_provider.dart';

class SpeciesRepository {
  final SpeciesProvider _provider;

  SpeciesRepository(this._provider);

  Future<List<Species>> loadData({required int id}) async =>
      await _provider.fetchData(id: id);
}
