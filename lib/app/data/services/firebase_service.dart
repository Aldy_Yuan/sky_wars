import 'package:firebase_core/firebase_core.dart';
import 'package:get/get.dart';

class FirebaseService extends GetxService {
  Future<FirebaseApp> init() async {
    return await Firebase.initializeApp();
  }
}