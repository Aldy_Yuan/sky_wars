import 'package:dio/dio.dart';
import 'package:sky_wars/app/data/models/species/species.dart';

class SpeciesProvider {
  Future<List<Species>> fetchData({required int id}) async {
    List<Species> data = [];
    final response =
        await Dio().get('https://swapi.dev/api/people/$id/?format=json');
    if (response.statusCode == 200 && response.data['species'] != null) {
      data = await fetchList(urls: (response.data['species'] as List<dynamic>));
    }

    return data;
  }

  Future<List<Species>> fetchList({required List<dynamic> urls}) async {
    List<Species> data = [];
    await Future.forEach(urls, (dynamic element) async {
      final response = await Dio().get(element);
      if (response.statusCode == 200) {
        data.add(Species.fromJson(response.data));
      }
    });
    return data;
  }
}
