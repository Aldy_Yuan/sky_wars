import 'package:sky_wars/app/data/models/people/people.dart';
import 'package:sky_wars/app/data/providers/db_provider.dart';
import 'package:sqflite/sqflite.dart';

class PeopleProvider {
  Future<List<People>> fetchData() async {
    Database db = await DBProvider().getDatabase();

    List<Map<String, dynamic>> data = await db.query('Peoples');

    List<People> result = data.map((e) => People.fromJson(e)).toList();

    return result;
  }

  Future<void> addData({required People value}) async {
    Database db = await DBProvider().getDatabase();

    await db.insert('Peoples', value.toJson());

    await db.close();
  }

  Future<void> deleteData({required int id}) async {
    Database db = await DBProvider().getDatabase();

    await db.delete('Peoples', where: 'rowid = ?', whereArgs: [id]);

    await db.close();
  }

  Future<void> updateData({required People value}) async {
    Database db = await DBProvider().getDatabase();

    await db.update('Peoples', value.toJson(),
        where: 'rowid = ?', whereArgs: [value.id]);

    await db.close();
  }
}
