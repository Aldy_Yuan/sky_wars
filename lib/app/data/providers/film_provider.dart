import 'package:dio/dio.dart';
import 'package:sky_wars/app/data/models/film/film.dart';

class FilmProvider {
  Future<List<Film>> fetchData({required int id}) async {
    List<Film> data = [];
    final response =
        await Dio().get('https://swapi.dev/api/people/$id/?format=json');
    if (response.statusCode == 200 && response.data['films'] != null) {
      data = await fetchList(urls: (response.data['films'] as List<dynamic>));
    }

    return data;
  }

  Future<List<Film>> fetchList({required List<dynamic> urls}) async {
    List<Film> data = [];
    await Future.forEach(urls, (dynamic element) async {
      final response = await Dio().get(element);
      if (response.statusCode == 200) {
        data.add(Film.fromJson(response.data));
      }
    });
    return data;
  }
}
