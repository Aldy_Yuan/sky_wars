import 'dart:convert';

import 'package:shared_preferences/shared_preferences.dart';
import 'package:sky_wars/app/data/models/people/people.dart';

class UserProvider {
  Future<List<People>> getFavorites() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    List<People> temp = [];

    if (prefs.containsKey("favorites")) {
      final dataJson = prefs.getString('favorites')!;
      final jsonValue = jsonDecode(dataJson);

      temp = (jsonValue as List).map((e) => People.fromJson(e)).toList();
    }

    return temp;
  }

  Future<void> saveUsers({
    required Map<String, dynamic> user,
  }) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    if (prefs.containsKey('user')) return;

    final jsonEncoded = jsonEncode(user);
    await prefs.setString('user', jsonEncoded);
  }

  Future<void> clearUsers() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    if (prefs.containsKey('user')) await prefs.remove('user');
  }

  Future<void> addFavorite({
    required List<People> list,
    required People value,
  }) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    list.add(value);
    final jsonEncoded = jsonEncode(list);
    await prefs.setString('favorites', jsonEncoded);
  }

  Future<void> deleteFavorite({
    required List<People> list,
    required int index,
  }) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    list.removeAt(index);
    final jsonEncoded = jsonEncode(list);
    await prefs.setString('favorites', jsonEncoded);
  }
}
