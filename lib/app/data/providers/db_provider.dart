import 'dart:io';

import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

class DBProvider {
  Future<Database> getDatabase() async {
    var databasesPath = await getDatabasesPath();
    var path = join(databasesPath, "starwars.db");

    try {
      await Directory(dirname(path)).create(recursive: true);
    } catch (_) {}

    Database db = await openDatabase(
      path,
      version: 1,
    );

    return db;
  }
}
