import 'package:get/get.dart';
import 'package:sky_wars/app/bindings/main_binding.dart';

import '../../main.dart';

class AppPages {
  static final pages = [
    GetPage(
      name: '/',
      page: () => const MyApp(),
      binding: MainBinding(),
    ),
  ];
}
