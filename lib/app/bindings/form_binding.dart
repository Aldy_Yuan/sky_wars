import 'package:get/get.dart';
import 'package:sky_wars/app/controllers/form_controller.dart';

class FormBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut<FormController>(() => FormController());
  }
}
