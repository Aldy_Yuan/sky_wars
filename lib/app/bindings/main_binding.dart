import 'package:get/get.dart';

import 'package:sky_wars/app/controllers/main_controller.dart';
import 'package:sky_wars/app/controllers/people_controller.dart';

import 'package:sky_wars/app/data/providers/people_provider.dart';

import 'package:sky_wars/app/data/repositories/people_repository.dart';

class MainBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut<MainController>(
      () => MainController(),
    );
    Get.lazyPut<PeopleController>(
      () => PeopleController(PeopleRepository(PeopleProvider())),
    );
  }
}
