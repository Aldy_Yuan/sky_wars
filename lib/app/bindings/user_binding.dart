import 'package:get/get.dart';
import 'package:sky_wars/app/controllers/user_controller.dart';

class UserBinding implements Bindings {
  @override
  void dependencies() {
    Get.put<UserController>(
      UserController(),
    );
  }
}
