import 'package:get/get.dart';
import 'package:sky_wars/app/controllers/film_controller.dart';
import 'package:sky_wars/app/controllers/species_controller.dart';
import 'package:sky_wars/app/data/providers/film_provider.dart';
import 'package:sky_wars/app/data/providers/species_provider.dart';
import 'package:sky_wars/app/data/repositories/film_repository.dart';
import 'package:sky_wars/app/data/repositories/species_repository.dart';

class DetailBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut<FilmController>(
      () => FilmController(FilmRepository(FilmProvider())),
    );
    Get.lazyPut<SpeciesController>(
      () => SpeciesController(SpeciesRepository(SpeciesProvider())),
    );
  }
}
