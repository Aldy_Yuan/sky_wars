import 'package:get/get.dart';
import 'package:sky_wars/app/controllers/db_controller.dart';

class DBControllerBinding implements Bindings {
  @override
  Future<void> dependencies() async {
    Get.putAsync<DBController>(() async => DBController());
  }
}
