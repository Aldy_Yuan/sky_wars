import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:sky_wars/app/bindings/form_binding.dart';
import 'package:sky_wars/app/controllers/film_controller.dart';
import 'package:sky_wars/app/controllers/people_controller.dart';
import 'package:sky_wars/app/controllers/species_controller.dart';
import 'package:sky_wars/app/data/models/film/film.dart';
import 'package:sky_wars/app/data/models/people/people.dart';
import 'package:sky_wars/app/pages/detail_film_page.dart';
import 'package:sky_wars/app/pages/detail_spesies_page.dart';

import 'add_people_page.dart';

class DetailPeoplePage extends StatelessWidget {
  final People people;
  const DetailPeoplePage({
    Key? key,
    required this.people,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final filmController = Get.find<FilmController>();
    final speciesController = Get.find<SpeciesController>();
    final peopleController = Get.find<PeopleController>();

    filmController.loadData(people.id!);
    speciesController.loadData(people.id!);

    return Scaffold(
      appBar: AppBar(
        title: const Text("Detail Orang"),
        actions: [
          IconButton(
            onPressed: () => Get.off(
              () => AddPeoplePage(
                people: people,
                isUpdate: true,
              ),
              binding: FormBinding(),
            ),
            icon: const Icon(Icons.edit_rounded),
          ),
          IconButton(
            onPressed: () async {
              await peopleController.deleteData(people);
            },
            icon: const Icon(Icons.delete_rounded),
          ),
          const SizedBox(width: 8.0),
        ],
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Card(
                child: ListTile(
                  title: Text(people.name!),
                  subtitle: Text(people.gender!),
                ),
              ),
              const SizedBox(height: 8.0),
              Card(
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const Text(
                        "Detail",
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 16,
                        ),
                      ),
                      const SizedBox(height: 16.0),
                      buildDescription('Tinggi', people.height!),
                      buildDescription('Massa', people.mass!),
                      buildDescription('Warna Kulit', people.skinColor!),
                      buildDescription('Warna Mata', people.eyeColor!),
                      buildDescription('Tgl Lahir', people.birthYear!),
                    ],
                  ),
                ),
              ),
              const SizedBox(height: 16.0),
              const Text(
                "Films",
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 16,
                ),
              ),
              Obx(
                () => filmController.isLoading
                    ? const Center(
                        child: CircularProgressIndicator(),
                      )
                    : filmController.films.isEmpty
                        ? const Center(
                            child: Text("Film kosong"),
                          )
                        : SizedBox(
                            height: 160,
                            child: ListView(
                              shrinkWrap: true,
                              scrollDirection: Axis.horizontal,
                              children: filmController.films
                                  .map((item) => buildFilmCard(context, item))
                                  .toList(),
                            ),
                          ),
              ),
              const SizedBox(height: 32.0),
              const Text(
                "Spesies",
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 16,
                ),
              ),
              const SizedBox(height: 16.0),
              Obx(
                () => speciesController.isLoading
                    ? const Center(
                        child: CircularProgressIndicator(),
                      )
                    : speciesController.species.isEmpty
                        ? const Center(
                            child: Text("Spesies kosong"),
                          )
                        : Column(
                            children: speciesController.species.map((e) {
                              return Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Card(
                                  child: ListTile(
                                    title: Text('${e.name}'),
                                    subtitle: Text("Bahasa: ${e.language}"),
                                    onTap: () => Get.to(
                                      () => DetailSpeciesPage(species: e),
                                    ),
                                    trailing:
                                        const Icon(Icons.navigate_next_rounded),
                                  ),
                                ),
                              );
                            }).toList(),
                          ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget buildDescription(String title, String desc) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Expanded(
          child: Text(
            title,
            style: const TextStyle(fontWeight: FontWeight.w500),
          ),
        ),
        Text(desc),
      ],
    );
  }

  Widget buildFilmCard(BuildContext context, Film item) {
    return InkWell(
      onTap: () => Get.to(
        () => DetailFilmPage(film: item),
      ),
      child: Container(
        width: 160,
        padding: const EdgeInsets.symmetric(
          horizontal: 8.0,
        ),
        child: Card(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Text(
                item.title!,
                style: const TextStyle(
                  fontWeight: FontWeight.bold,
                ),
                textAlign: TextAlign.center,
              ),
              const SizedBox(
                height: 6.0,
              ),
              Text(
                "Direktur:\n${item.director}",
                textAlign: TextAlign.center,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
