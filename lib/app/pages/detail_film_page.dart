import 'package:flutter/material.dart';
import 'package:sky_wars/app/data/models/film/film.dart';

class DetailFilmPage extends StatelessWidget {
  final Film film;
  const DetailFilmPage({
    Key? key,
    required this.film,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Detail Film"),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            children: [
              ListTile(
                subtitle: const Text("Judul"),
                title: Text("${film.title}"),
              ),
              ListTile(
                subtitle: const Text("Opening"),
                title: Text("${film.openingCrawl}"),
              ),
              ListTile(
                subtitle: const Text("Tanggal Rilis"),
                title: Text("${film.releaseDate}"),
              ),
              ListTile(
                subtitle: const Text("Produser"),
                title: Text("${film.producer}"),
              ),
              ListTile(
                subtitle: const Text("Direktur"),
                title: Text("${film.director}"),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
