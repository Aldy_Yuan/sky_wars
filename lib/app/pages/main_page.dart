import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:sky_wars/app/controllers/main_controller.dart';
import 'package:sky_wars/app/screens/favorite_screen.dart';
import 'package:sky_wars/app/screens/home_screen.dart';
import 'package:sky_wars/app/screens/profile_screen.dart';

class MainPage extends StatelessWidget {
  const MainPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final List<Widget> _buildScreens = [
      const HomeScreen(),
      const FavoriteScreen(),
      const ProfileScreen(),
    ];

    final MainController mainController = Get.find<MainController>();

    final List<BottomNavigationBarItem> items = [
      const BottomNavigationBarItem(
        icon: Icon(Icons.home_rounded),
        activeIcon: Icon(Icons.home_rounded, size: 28),
        label: 'Beranda',
      ),
      const BottomNavigationBarItem(
        icon: Icon(Icons.favorite_rounded),
        activeIcon: Icon(Icons.favorite_rounded, size: 28),
        label: 'Favorit',
      ),
      const BottomNavigationBarItem(
        icon: Icon(Icons.account_circle_rounded),
        activeIcon: Icon(Icons.account_circle_rounded, size: 28),
        label: 'Profil',
      ),
    ];

    return Scaffold(
      backgroundColor: Colors.transparent,
      body: Obx(
        () => _buildScreens[mainController.currentIndex],
      ),
      bottomNavigationBar: Obx(
        () => BottomNavigationBar(
          elevation: 4,
          currentIndex: mainController.currentIndex,
          onTap: (value) => mainController.setCurrentIndex = value,
          showSelectedLabels: false,
          showUnselectedLabels: false,
          unselectedIconTheme: const IconThemeData(
            color: Colors.blueGrey,
          ),
          selectedItemColor: Colors.blue,
          backgroundColor: Colors.white,
          items: items,
        ),
      ),
    );
  }
}
