import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:sky_wars/app/controllers/form_controller.dart';
import 'package:sky_wars/app/controllers/people_controller.dart';
import 'package:sky_wars/app/data/models/people/people.dart';

class AddPeoplePage extends StatelessWidget {
  final bool isUpdate;
  final People? people;
  const AddPeoplePage({
    Key? key,
    required this.isUpdate,
    required this.people,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
    final _controller = Get.find<FormController>();
    final _people = Get.find<PeopleController>();

    if (isUpdate) _controller.initData(people!);

    return Scaffold(
      appBar: AppBar(
        title: const Text("Tambah Orang"),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Form(
            key: _formKey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                const SizedBox(height: 16.0),
                TextFormField(
                  decoration: const InputDecoration(
                    labelText: 'Nama',
                  ),
                  initialValue: _controller.name,
                  textCapitalization: TextCapitalization.words,
                  onChanged: (value) => _controller.setName = value,
                  validator: (value) {
                    if (value!.isEmpty) {
                      return 'You must fill this field';
                    }

                    return null;
                  },
                ),
                const SizedBox(height: 16.0),
                TextFormField(
                  decoration: const InputDecoration(
                    labelText: 'Tinggi',
                  ),
                  keyboardType: TextInputType.number,
                  initialValue: _controller.height,
                  inputFormatters: [
                    FilteringTextInputFormatter.digitsOnly,
                  ],
                  onChanged: (value) => _controller.setHeight = value,
                  validator: (value) {
                    if (value!.isEmpty) {
                      return 'You must fill this field';
                    }

                    return null;
                  },
                ),
                const SizedBox(height: 16.0),
                TextFormField(
                  decoration: const InputDecoration(
                    labelText: 'Massa',
                  ),
                  keyboardType: TextInputType.number,
                  initialValue: _controller.mass,
                  inputFormatters: [
                    FilteringTextInputFormatter.digitsOnly,
                  ],
                  onChanged: (value) => _controller.setMass = value,
                  validator: (value) {
                    if (value!.isEmpty) {
                      return 'You must fill this field';
                    }

                    return null;
                  },
                ),
                const SizedBox(height: 16.0),
                TextFormField(
                  decoration: const InputDecoration(
                    labelText: 'Warna kulit',
                  ),
                  textCapitalization: TextCapitalization.sentences,
                  initialValue: _controller.skinColor,
                  onChanged: (value) => _controller.setSkinColor = value,
                  validator: (value) {
                    if (value!.isEmpty) {
                      return 'You must fill this field';
                    }

                    return null;
                  },
                ),
                const SizedBox(height: 16.0),
                TextFormField(
                  decoration: const InputDecoration(
                    labelText: 'Warna Mata',
                  ),
                  initialValue: _controller.eyeColor,
                  textCapitalization: TextCapitalization.sentences,
                  onChanged: (value) => _controller.setEyeColor = value,
                  validator: (value) {
                    if (value!.isEmpty) {
                      return 'You must fill this field';
                    }

                    return null;
                  },
                ),
                const SizedBox(height: 16.0),
                TextFormField(
                  decoration: const InputDecoration(
                    labelText: 'Tanggal Lahir',
                  ),
                  initialValue: _controller.birth,
                  textCapitalization: TextCapitalization.sentences,
                  onChanged: (value) => _controller.setBirth = value,
                  validator: (value) {
                    if (value!.isEmpty) {
                      return 'You must fill this field';
                    }

                    return null;
                  },
                ),
                const SizedBox(height: 16.0),
                TextFormField(
                  decoration: const InputDecoration(
                    labelText: 'Jenis Kelamin',
                  ),
                  initialValue: _controller.gender,
                  textCapitalization: TextCapitalization.sentences,
                  onChanged: (value) => _controller.setGender = value,
                  validator: (value) {
                    if (value!.isEmpty) {
                      return 'You must fill this field';
                    }

                    return null;
                  },
                ),
                const SizedBox(height: 56.0),
                Obx(
                  () => _controller.loading
                      ? const Center(
                          child: CircularProgressIndicator(),
                        )
                      : isUpdate
                          ? MaterialButton(
                              onPressed: () async {
                                if (!_formKey.currentState!.validate()) {
                                  return;
                                }

                                _controller.setLoading = true;

                                People data = People(
                                    people!.id,
                                    _controller.name,
                                    _controller.height,
                                    _controller.mass,
                                    _controller.skinColor,
                                    _controller.eyeColor,
                                    _controller.birth,
                                    _controller.gender,
                                    "",
                                    favorite: people!.favorite);

                                await _people.updateData(data);

                                _controller.setLoading = false;
                              },
                              child: const Text("Update orang"),
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(32.0),
                              ),
                              color: Colors.deepOrange,
                              textColor: Colors.white,
                              padding: const EdgeInsets.all(14.0),
                            )
                          : ElevatedButton(
                              onPressed: () async {
                                if (!_formKey.currentState!.validate()) {
                                  return;
                                }

                                _controller.setLoading = true;

                                People data = People(
                                  null,
                                  _controller.name,
                                  _controller.height,
                                  _controller.mass,
                                  _controller.skinColor,
                                  _controller.eyeColor,
                                  _controller.birth,
                                  _controller.gender,
                                  "",
                                );

                                await _people.insertData(data).whenComplete(() {
                                  Get.back();
                                  Get.snackbar(
                                    'Berhasil',
                                    'Orang ditambahkan',
                                    duration: const Duration(seconds: 3),
                                  );
                                });

                                _controller.setLoading = false;
                              },
                              child: const Text(
                                'Tambah Orang',
                              ),
                            ),
                ),
                const SizedBox(height: 56.0),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
