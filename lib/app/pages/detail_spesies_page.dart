import 'package:flutter/material.dart';
import 'package:sky_wars/app/data/models/species/species.dart';

class DetailSpeciesPage extends StatelessWidget {
  final Species species;
  const DetailSpeciesPage({
    Key? key,
    required this.species,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Detail Spesies"),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            children: [
              ListTile(
                subtitle: const Text("Nama"),
                title: Text("${species.name}"),
              ),
              ListTile(
                subtitle: const Text("Bahasa"),
                title: Text("${species.language}"),
              ),
              ListTile(
                subtitle: const Text("Tinggi rata-rata"),
                title: Text("${species.averageHeight}"),
              ),
              ListTile(
                subtitle: const Text("Umur rata-rata"),
                title: Text("${species.averageLifespan}"),
              ),
              ListTile(
                subtitle: const Text("Klasifikasi"),
                title: Text("${species.classification}"),
              ),
              ListTile(
                subtitle: const Text("Warna Mata"),
                title: Text("${species.eyeColors}"),
              ),
              ListTile(
                subtitle: const Text("Warna Rambut"),
                title: Text("${species.hairColors}"),
              ),
              ListTile(
                subtitle: const Text("Warna Kulit"),
                title: Text("${species.skinColors}"),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
