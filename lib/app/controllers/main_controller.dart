import 'package:get/get.dart';

class MainController extends GetxController {
  MainController();

  final Rx<int> _currentIndex = 0.obs;
  set setCurrentIndex(value) => _currentIndex.value = value;
  int get currentIndex => _currentIndex.value;
}
