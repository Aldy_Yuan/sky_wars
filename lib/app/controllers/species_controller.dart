import 'package:get/get.dart';

import 'package:sky_wars/app/data/models/species/species.dart';

import 'package:sky_wars/app/data/repositories/species_repository.dart';

class SpeciesController extends GetxController {
  final SpeciesRepository repository;
  SpeciesController(this.repository);

  final Rx<List<Species>> _species = Rx<List<Species>>([]);
  List<Species> get species => _species.value;

  final Rx<bool> _isLoading = false.obs;
  bool get isLoading => _isLoading.value;

  loadData(int id) async {
    _isLoading.value = true;
    _species.value.assignAll(await repository.loadData(id: id));
    _isLoading.value = false;
  }
}
