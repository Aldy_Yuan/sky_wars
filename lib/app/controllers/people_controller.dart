import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:sky_wars/app/data/models/people/people.dart';
import 'package:sky_wars/app/data/repositories/people_repository.dart';
import 'package:sky_wars/widgets/confirmation.dart';

class PeopleController extends GetxController {
  final PeopleRepository repository;
  PeopleController(this.repository);

  final Rx<List<People>> _peoples = Rx<List<People>>([]);
  List<People> get peoples => _peoples.value;

  final Rx<List<People>> _searchPeoples = Rx<List<People>>([]);
  List<People> get searchPeoples => _searchPeoples.value;

  final Rx<List<People>> _searchPeoplesFavorite = Rx<List<People>>([]);
  List<People> get searchPeoplesFavorite => _searchPeoplesFavorite.value;

  final Rx<List<People>> _favoritePeople = Rx<List<People>>([]);
  List<People> get favoritePeople => _favoritePeople.value;

  final TextEditingController text = TextEditingController();

  final Rx<bool> _isLoading = false.obs;
  bool get isLoading => _isLoading.value;

  final Rx<bool> _isEmpty = false.obs;
  bool get isEmpty => _isEmpty.value;

  final Rx<bool> _isList = true.obs;
  bool get isList => _isList.value;
  set setIsList(value) => _isList.value = value;

  final Rx<int> _sortValue = 0.obs;
  int get sortValue => _sortValue.value;
  set setSortValue(value) => _sortValue.value = value;

  final Rx<int> _favoriteSort = 0.obs;
  int get favoriteSort => _favoriteSort.value;
  set setFavoriteSort(value) => _favoriteSort.value = value;

  loadData() async {
    _isLoading.value = true;
    List<People> items = await repository.loadData();
    items.sort(
      (a, b) => a.name!.compareTo(b.name!),
    );
    _peoples.value.assignAll(items);
    _favoritePeople.value.assignAll(
      items.where((element) => element.favorite == 1),
    );
    _isLoading.value = false;
  }

  @override
  void onInit() {
    loadData();

    super.onInit();
  }

  Future<void> addFavorite(People data) async {
    People value = People(
      data.id,
      data.name,
      data.height,
      data.mass,
      data.skinColor,
      data.eyeColor,
      data.birthYear,
      data.gender,
      "",
      favorite: 1,
    );
    await repository.updateData(value);
    refreshFavorite(data.id!, 1);
  }

  Future<void> deleteFavorite(People data) async {
    People value = People(
      data.id,
      data.name,
      data.height,
      data.mass,
      data.skinColor,
      data.eyeColor,
      data.birthYear,
      data.gender,
      "",
      favorite: 0,
    );
    await repository.updateData(value);
    refreshFavorite(data.id!, 0);
  }

  refreshFavorite(int id, int favorite) {
    int index = _peoples.value.indexWhere((element) => element.id == id);
    _peoples.value[index].favorite = favorite;
    _favoritePeople.value.assignAll(
      _peoples.value.where((element) => element.favorite == 1),
    );
    _favoritePeople.refresh();
    _peoples.refresh();
  }

  Future<void> insertData(People data) async {
    _isLoading.value = true;
    await repository.insertData(data);
    await loadData();
    _isLoading.value = false;
  }

  Future<void> updateData(People data) async {
    _isLoading.value = true;
    bool? confirmation = await Get.dialog(
      const Confirmation(
        title: 'Update Data',
        content: "Anda akan mengupdate data orang ini.",
      ),
      barrierDismissible: false,
    );
    if (confirmation!) {
      await repository.updateData(data);
      await loadData();
      Get.back();
      Get.snackbar(
        'Berhasil',
        'Orang diupdate',
        duration: const Duration(seconds: 3),
      );
    }
    _isLoading.value = false;
  }

  Future<void> deleteData(People data) async {
    _isLoading.value = true;
    bool? confirmation = await Get.dialog(
      const Confirmation(
        title: 'Delete Data',
        content: "Anda akan menghapus data orang ini.",
      ),
      barrierDismissible: false,
    );
    if (confirmation!) {
      await repository.deleteData(data.id!);
      await loadData();
      Get.back();
      Get.snackbar(
        'Berhasil',
        'Orang dihapus',
        duration: const Duration(seconds: 3),
      );
    }
    _isLoading.value = false;
  }

  sortAllPeoples() {
    switch (_sortValue.value) {
      case 0:
        _peoples.value.sort(
          (a, b) => a.name!.compareTo(b.name!),
        );

        break;
      case 1:
        _peoples.value.sort(
          (a, b) => b.name!.compareTo(a.name!),
        );

        break;
      default:
        _peoples.value.sort(
          (a, b) => a.name!.compareTo(b.name!),
        );
    }
    _peoples.refresh();
  }

  search(String query) async {
    _isLoading.value = true;
    if (query.isEmpty || query == "") {
      _searchPeoples.value.clear();
      _isEmpty.value = false;
      _searchPeoples.refresh();
      _isLoading.value = false;
      return;
    }

    _searchPeoples.value.clear();
    _searchPeoples.value.assignAll(
        _peoples.value.where((element) => element.name!.contains(query)));

    if (_searchPeoples.value.isEmpty) {
      _isEmpty.value = true;
    }
    _isLoading.value = false;
  }

  searchFavorite(String query) async {
    _isLoading.value = true;
    if (query.isEmpty || query == "") {
      _searchPeoplesFavorite.value.clear();
      _isEmpty.value = false;
      _searchPeoplesFavorite.refresh();
      _isLoading.value = false;
      return;
    }

    _searchPeoplesFavorite.value.clear();
    _searchPeoplesFavorite.value.assignAll(_favoritePeople.value
        .where((element) => element.name!.contains(query)));

    if (_searchPeoplesFavorite.value.isEmpty) {
      _isEmpty.value = true;
    }
    _isLoading.value = false;
  }

  sortAllFavorite() {
    switch (_favoriteSort.value) {
      case 0:
        _favoritePeople.value.sort(
          (a, b) => a.name!.compareTo(b.name!),
        );

        break;
      case 1:
        _favoritePeople.value.sort(
          (a, b) => b.name!.compareTo(a.name!),
        );

        break;
      default:
        _favoritePeople.value.sort(
          (a, b) => a.name!.compareTo(b.name!),
        );
    }
    _favoritePeople.refresh();
  }
}
