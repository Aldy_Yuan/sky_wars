import 'package:get/get.dart';
import 'package:sky_wars/app/data/models/film/film.dart';
import 'package:sky_wars/app/data/repositories/film_repository.dart';

class FilmController extends GetxController {
  final FilmRepository repository;
  FilmController(this.repository);

  final Rx<List<Film>> _films = Rx<List<Film>>([]);
  List<Film> get films => _films.value;

  final Rx<bool> _isLoading = false.obs;
  bool get isLoading => _isLoading.value;

  loadData(int id) async {
    _isLoading.value = true;
    List<Film> value = await repository.loadData(id: id);
    _films.value.assignAll(value);
    _isLoading.value = false;
  }
}
