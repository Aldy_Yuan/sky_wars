import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_sign_in/google_sign_in.dart';

import 'package:sky_wars/app/data/providers/user_provider.dart';
import 'package:sky_wars/app/data/repositories/user_repository.dart';
import 'package:sky_wars/widgets/confirmation.dart';

class UserController extends GetxController with StateMixin<User?> {
  final _auth = FirebaseAuth.instance;

  final Rxn<User> _user = Rxn<User>();

  final userRepository = UserRepository(UserProvider());

  final Rx<bool> _isLoading = false.obs;
  bool get loading => _isLoading.value;

  @override
  void onInit() async {
    RxStatus.loading();
    _user.bindStream(_auth.userChanges());

    ever<User?>(_user, (user) async {
      RxStatus.loading();

      if (user == null) {
        change(null, status: RxStatus.empty());
        return;
      }

      await userRepository.saveUser(user: {
        'uid': user.uid,
        'email': user.email,
        'display_name': user.displayName,
        'photo_url': user.photoURL,
      });

      change(
        user,
        status: RxStatus.success(),
      );
    });
    super.onInit();
  }

  signInWithGoogle() async {
    _isLoading.value = true;
    // Trigger the authentication flow
    final GoogleSignInAccount? googleUser = await GoogleSignIn().signIn();

    if (googleUser == null) return;

    // Obtain the auth details from the request
    final GoogleSignInAuthentication googleAuth =
        await googleUser.authentication;

    // Create a new credential
    final credential = GoogleAuthProvider.credential(
      accessToken: googleAuth.accessToken,
      idToken: googleAuth.idToken,
    );

    try {
      // Once signed in, return the UserCredential
      await _auth.signInWithCredential(credential);

      Get.offAllNamed('/');
    } on FirebaseAuthException catch (e) {
      Get.snackbar(
        'Error',
        '${e.message}',
        backgroundColor: Colors.red,
        colorText: Colors.white,
      );
    }
  }

  logout() async {
    bool? confirmation = await Get.dialog(
      const Confirmation(
        title: 'Logout',
        content: "Anda akan keluar dari akun Anda.",
      ),
      barrierDismissible: false,
    );

    if (confirmation!) {
      await userRepository.clearUser();
      await _auth.signOut();

      change(null, status: RxStatus.empty());
    }
  }
}
