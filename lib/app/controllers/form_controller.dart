import 'package:get/get.dart';
import 'package:sky_wars/app/data/models/people/people.dart';

class FormController extends GetxController {
  FormController();

  final Rx<String> _name = ''.obs;
  set setName(value) => _name.value = value;
  String get name => _name.value;

  final Rx<String> _height = ''.obs;
  set setHeight(value) => _height.value = value;
  String get height => _height.value;

  final Rx<String> _mass = ''.obs;
  set setMass(value) => _mass.value = value;
  String get mass => _mass.value;

  final Rx<String> _skinColor = ''.obs;
  set setSkinColor(value) => _skinColor.value = value;
  String get skinColor => _skinColor.value;

  final Rx<String> _eyeColor = ''.obs;
  set setEyeColor(value) => _eyeColor.value = value;
  String get eyeColor => _eyeColor.value;

  final Rx<String> _birth = ''.obs;
  set setBirth(value) => _birth.value = value;
  String get birth => _birth.value;

  final Rx<String> _gender = ''.obs;
  set setGender(value) => _gender.value = value;
  String get gender => _gender.value;

  final Rx<bool> _isLoading = false.obs;
  set setLoading(value) => _isLoading.value = value;
  bool get loading => _isLoading.value;

  initData(People data) {
    _name.value = data.name!;

    _height.value = data.height!;

    _mass.value = data.mass!;

    _skinColor.value = data.skinColor!;

    _eyeColor.value = data.eyeColor!;

    _birth.value = data.birthYear!;

    _gender.value = data.gender!;
  }
}
