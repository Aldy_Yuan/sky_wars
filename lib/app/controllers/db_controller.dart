import 'dart:async';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:get/get.dart';
import 'package:path/path.dart';
import 'package:sky_wars/app/data/models/people/people.dart';
import 'package:sqflite/sqflite.dart';

class DBController extends GetxController {
  final Rx<bool> _isLoading = false.obs;
  bool get isLoading => _isLoading.value;

  final List<People> _result = [];

  @override
  void onInit() {
    initDB();
    super.onInit();
  }

  Future<void> initDB() async {
    _isLoading.value = true;
    var databasesPath = await getDatabasesPath();
    var path = join(databasesPath, "starwars.db");

    // Delete the database
    // await deleteDatabase(path);

    try {
      await Directory(dirname(path)).create(recursive: true);
    } catch (_) {}

    if (await databaseExists(path)) {
      _isLoading.value = false;
      return;
    }

    await openDatabase(path, version: 1, onOpen: (db) {},
        onCreate: (Database db, int version) async {
      await db.execute("CREATE TABLE Peoples ("
          "rowid INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
          "name TEXT,"
          "height TEXT,"
          "mass TEXT,"
          "skin_color TEXT,"
          "eye_color TEXT,"
          "birth_year TEXT,"
          "gender TEXT,"
          "url TEXT,"
          "favorite INTEGER"
          ")");

      List<People> data =
          await _getApiData("https://swapi.dev/api/people/?format=json");

      await Future.forEach(
        data,
        (People element) async => await db.insert(
          "Peoples",
          element.toJson(),
        ),
      );
    });

    _isLoading.value = false;

    return;
  }

  Future<List<People>> _getApiData(String path) async {
    final response = await Dio().get(path);

    if (response.statusCode == 200) {
      List<People> temp = (response.data['results'] as List)
          .map((e) => People.fromJson(e))
          .toList();

      _result.addAll(temp);

      if (response.data['next'] != null) {
        await _getApiData(response.data['next']);
      }
    }

    return _result;
  }
}
