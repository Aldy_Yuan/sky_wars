import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'package:sky_wars/app/controllers/db_controller.dart';
import 'package:sky_wars/app/controllers/people_controller.dart';

import 'package:sky_wars/widgets/people_card.dart';
import 'package:sky_wars/widgets/people_grid.dart';

class FavoriteScreen extends StatelessWidget {
  const FavoriteScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final PeopleController _peopleController = Get.find<PeopleController>();
    final DBController _dbController = Get.find<DBController>();

    return Scaffold(
      body: SafeArea(
        child: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.all(16.0),
            child: Column(
              children: [
                const SizedBox(height: 8.0),
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    Expanded(
                      child: TextFormField(
                        decoration: const InputDecoration(
                          prefixIcon: Icon(Icons.search_rounded),
                          hintText: 'Cari disini',
                        ),
                        controller: _peopleController.text,
                        onChanged: (String value) =>
                            _peopleController.searchFavorite(value),
                      ),
                    ),
                    _buildToggleButtons(context, _peopleController),
                    const SizedBox(width: 8.0),
                    _buildDropDown(context, _peopleController),
                    const SizedBox(width: 8.0),
                  ],
                ),
                const SizedBox(height: 16.0),
                Obx(
                  () => _dbController.isLoading || _peopleController.isLoading
                      ? const Center(
                          child: CircularProgressIndicator(),
                        )
                      : _peopleController.isEmpty
                          ? const Center(
                              child: Text("Pencarian kosong"),
                            )
                          : _peopleController.favoritePeople.isEmpty
                              ? const Center(
                                  child: Text("Favorite masih kosong"),
                                )
                              : _peopleController.isList
                                  ? Column(
                                      children: (_peopleController
                                                  .searchPeoplesFavorite.isEmpty
                                              ? _peopleController.favoritePeople
                                              : _peopleController
                                                  .searchPeoplesFavorite)
                                          .map((e) => PeopleCard(people: e))
                                          .toList(),
                                    )
                                  : GridView.count(
                                      crossAxisCount: 3,
                                      mainAxisSpacing: 4,
                                      crossAxisSpacing: 4,
                                      shrinkWrap: true,
                                      physics:
                                          const NeverScrollableScrollPhysics(),
                                      children: (_peopleController
                                                  .searchPeoplesFavorite.isEmpty
                                              ? _peopleController.favoritePeople
                                              : _peopleController
                                                  .searchPeoplesFavorite)
                                          .map((e) => PeopleGrid(people: e))
                                          .toList(),
                                    ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildToggleButtons(
      BuildContext context, PeopleController controller) {
    return Obx(
      () => controller.isList
          ? IconButton(
              onPressed: () => controller.setIsList = false,
              icon: const Icon(Icons.list_rounded, color: Colors.blue),
            )
          : IconButton(
              onPressed: () => controller.setIsList = true,
              icon: const Icon(Icons.grid_view_rounded, color: Colors.blue),
            ),
    );
  }

  Widget _buildDropDown(BuildContext context, PeopleController controller) {
    return Obx(
      () => DropdownButton<int>(
        items: const [
          DropdownMenuItem(
            value: 0,
            child: Text('A - Z'),
          ),
          DropdownMenuItem(
            value: 1,
            child: Text('Z - A'),
          ),
        ],
        icon: const Icon(
          Icons.expand_more_rounded,
          color: Colors.blue,
        ),
        style: const TextStyle(
          color: Colors.blue,
          fontWeight: FontWeight.bold,
        ),
        underline: const SizedBox(),
        value: controller.favoriteSort,
        onChanged: (value) {
          controller.setFavoriteSort = value;

          if (controller.peoples.isNotEmpty) {
            controller.sortAllFavorite();
          }
        },
      ),
    );
  }
}
